# Savez-vous programmer ? 


> 1991 - Dutch programmer Guido van Rossum travels to Argentina for a mysterious
> operation. He returns  with a large cranial scar, invents  Python, is declared
> Dictator for  Life by legions  of followers, and  announces to the  world that
> « There Is Only One Way to Do It! » Poland becomes nervous.

> in « A brief, incomplete, and  mostly wrong history of programming languages »
> de James IRY


>Un enseignement d'informatique pour tous  est officiellement entré en vigueur à
>la rentrée 2013.   À l'heure où nous imprimons, le langage retenu pour
> illustrer cet  enseignement est Python  que ce soit en  MP, MPI, PC,  PSI, PT,
> TSI, BCPST ou ECG. On présentera  également dans ces sections une introduction
> aux  bases de  données et  au  langage SQL.  Les  MPI étudieront  en plus  les
> langages OCAML et C.




## Avant de commencer

### Python

`Python` est un vrai langage de programmation. Il est naturellement
disponible  sur  toute  distribution  GNU-Linux et  vous  pouvez  même
l'utiliser sur les autres systèmes d'exploitation.  C'est un
langage de très haut niveau dont la syntaxe encourage à écrire du code clair et
de qualité. Dans le domaine de la gestion de la mémoire, les détails de
bas niveau de langages comme le C disparaissent. De plus l'apprentissage de `Python`
est facilité par l'existence d'une interface interactive.  Ceci dit, son intérêt
ne se réduit pas à l'apprentissage de la programmation ou de l'algorithmique;
en témoigne sa popularité croissante. Il a été choisi par des acteurs majeurs:
Google, YouTube, la NASA, etc.  Il
favorise la programmation impérative structurée et la programmation orientée
objet; dans une moindre mesure, il permet de programmer dans un style
fonctionnel. Il est doté
d'un typage dynamique fort, d'une gestion automatique de la mémoire par
ramasse-miettes et d'un système de gestion d'exceptions.  C'est un langage
multi-plateforme, polyvalent (jusque dans les domaines comme le web, les
graphiques, le réseau), open source et gratuit.

En `Python`, tout est objet mais la programmation orientée objet n'est vue que de
manière optionnelle en prépa.


`Python` est accompagné d'un très grand nombre de bibliothèques. Les plus 
utiles   pour  le   programme  de   prépa  seront  `NumPy`,
`SciPy`,   `Matplotlib`  qui   permettent  de   faire  du   calcul
scientifique.



### Environnement de travail


`Python` est  un langage  qui dispose  d'un interpréteur  très pratique
pour tester de petits bout de code.

On  distinguera donc  un fichier  texte d'extension `.py` dans
lequel on écrira  nos diverses lignes de code  que nous enregistrerons
et pourrons  utiliser à diverses  occasions et nos   « dialogues »
avec  `Python`. Cette  approche intuitive  suffit pour  l'instant: vous
parlerez plus  tard de  langage compilé,  interprété, semi-interprété,
**bytecode**, **opcode**, etc.


Il  existe  de  très  nombreux environnements  de  travail  adaptés  à
`Python`.
Nous ne parlerons que de `iPython` qui est utilisé dans les distributions `Pyzo`
(`https://www.pyzo.org/`) et
  `Spyder` (`https://pythonhosted.org/spyder/`) le  plus souvent disponibles aux
  concours et dans tous les lycées. 


Pour installer `Spyder` par exemple, il suffit d'aller dans un terminal : « l'écran
noir » :)


```shell
python3.X -m pip install spyder 
```

vous remplacerez le X par la version de `Python` disponible sur votre machine et
eventuellement suivi de  `.exe` si vous êtes  sur Windows. Dans la  suite de cet
ouvrage nous utiliserons `Python3.9`. 

```shell
Python 3.9.2 (default, Mar 22 2021, 14:20:33) 
Type 'copyright', 'credits' or 'license' for more information
IPython 7.21.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: 
```

Et voici l'aspect de `Spyder`:

![spyder](./IMG/spyder.png)



## Premiers pas

### Python comme super-calculatrice


Si l'on veut faire des calculs simples,  on peut aller dans la console `iPython`
de son environnement `Spyder`.

On importe au besoin certaines bibliothèques de calcul comme par exemple 
`sympy`    pour  le  calcul
symbolique, `scipy` et `numpy` pour le calcul numérique, à partir de la console `iPython` avec la commande **magique**
`%pip`:

```shell
In[1]: %pip install scipy

Defaulting to user installation because normal site-packages is not writeable
Collecting scipy
  Downloading scipy-1.6.2-cp39-cp39-manylinux1_x86_64.whl (27.3 MB)
     |████████████████████████████████| 27.3 MB 2.2 MB/s eta 0:00:01
irement already satisfied: numpy<1.23.0,>=1.16.5 in ./.local/lib/python3.9/site-packages (from scipy) (1.20.1)
Installing collected packages: scipy
Successfully installed scipy-1.6.2
Note: you may need to restart the kernel to use updated packages.
```

On utilisera aussi `matplotlib.pyplot` pour les tracés de courbes.

```python
import numpy as np
import matplotlib.pyplot as plt
import sympy as sy

np.sqrt(2)
Out[7]: 1.4142135623730951

sy.sqrt(2)
Out[8]: 
sqrt(2)

np.sqrt(2)**2
Out[9]: 2.0000000000000004

sy.sqrt(2)**2
Out[10]: 
2
```


## Un peu de vocabulaire


Quand on écrit:

```python
var1 = "Point n'est besoin d'espérer"
var2 = " pour entreprendre"
var3 = var1 + var2
```
On crée trois **objets** de **type** `str`, *"Point n'est besoin d'espérer"* et 
*"pour  entreprendre"*  sont deux  **constantes**  (*littéraux*  en franglais)  et
`var1  + var2`  une **expression**.  Cela permet  de **lier**  les **variables**
`var1`, `var2`  et `var3`  à ces  constantes ou expressions.  On fait  ainsi des
**affectations**.


**Remarque**


En Python, une expression est évaluée avant d'être affectée.

Par exemple en Python:
```python
>>> a = 0
>>> b = 2
>>> c = (1, b/a)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```




### Constante

C'est la représentation  en code Python d'une valeur d'un  certain type. `1` est
une constante de type `int`, `True` est une constante de type `bool`, etc.

### Opérateur

C'est  la représentation  en code  Python d'une  opération sur  des objets  d'un
certain type : par exemple `+` est l'opérateur représentant l'opération *addition des `int`* mais c'est
aussi l'opérateur représentant l'opération *concaténation de deux `str`*.



### Identifiant

C'est la représentation  en code Python d'un  nom qui est associé  à une adresse
mémoire. Les  identifiants sont des suites  de lettres, de chiffres  et de tiets
bas, le premier caractère ne pouvant être un chiffre.

Attention à  ne pas choisir  comme identifiant des  mots-clés ou de  types comme
`while`, `int`, etc.

### Variable

Une variable est  un nom associé à une valeur  pour faciliter son identifiaction
par le programmeur et dont la valeur peut changer au cours du programme. 

### Expression

Une expression est une combinaison  de constantes, variables et d'opérateurs que
Python va *évaluer*, i.e. Python va produire une valeur associée à cette expression.


### Affectation

Quand on  écrit `d =  1` en Python, on  n'exprime pas une  identité mathématique
mais une **action** qui consiste à :

* définir un *identifiant* `d` comme une nouvelle *variable*
* associer à cette variable un entier dont la *valeur* est 1.


```mermaid
graph TD;
A("label d")-->B["int 1"]
```



On fait bien cette distinction dans d'autres langages comme `OCaml`:

```ocaml
# let d = ref 1;;
val d : int ref = {contents = 1}

# d;;
- : int ref = {contents = 1}

# !d;;
- : int = 1
```

Quand on écrit:

```python
a = 12
t = a
```

on a  deux variables `a` et  `t` qui pointent vers  le même objet `12`  (qui est
ici un `int`).


### Objet

Toutes les données  d'un programme Python sont des objets  qui sont caractérisés
par:

* leur **identifiant**:  en gros, cela indique un numéro  qui identifie une case
  mémoire.
  
* leur **type**:  cela permet de connaître le comportement  de l'objet, ce qu'il
  peut représenter et ce qu'on peut lui faire faire.
  
* leur **valeur**: la donnée qu'il représente.



### Référence d'un objet

En  fait,  les  noms qu'on  donne  aux  objets  sont  juste des  liens  vers  les
objets. Écrire `a = b` signifie qu'on copie l'adresse `a` dans `b`. 

La fonction `id`  nous permet d'avoir un *identifiant* unique  pour un objet qui
sera  unique  tant  que  cet  objet restera  "vivant"  (la  plupart  des  objets
disparaissent dans le *ramasse-miettes*).  C'est comme un numéro de
sécurité sociale.





**Observez, commentez, expliquez**

```python
>>> a = 1                                                            

>>> b = a                                                            

>>> id(a)                                                            
 93835776299776

>>> id(b)                                                            
 93835776299776
```

```mermaid
graph TD;
  a("a")-->1;
  b("b")-->1;
```




```python
>>> b = 2 

>>> id(b)                                                            
 93835776299808

>>> id(a)                                                            
 93835776299776
```



```mermaid
graph TD;
  a("a")-->1;
  b("b")-->2;
```


```python
>>> a = b

>>> id(a)                                                            
 93835776299808
```
  


```mermaid
graph TD;
	       1;
  a("a")-->2;
  b("b")-->2;
```



### Et dans d'autres langages ?




D'autres langages n'ont carrément pas d'affectation comme Haskell qui a inspiré
de nombreux langages parmi les plus utilisés actuellement. 

On ne peut pas écrire `a = a + 1`.

Nous en  rediscuterons plus tard  mais on essaiera  de recourir le  plus souvent
possible aux **affectations uniques**.

On évitera les `a = a + a` qui rendent la lecture du code difficile.

On préfèrera utiliser une nouvelle variable.


Quant à l'utilisation du symbole `=` pour l'affectation, voici ce qu'en dit Niklaus Wirth

*A notorious  example for a  bad idea  was the choice  of the equal  sign to
    denote assignment. It  goes back to Fortran in 1957[a]  and has blindly been
    copied by  armies of language  designers. Why is it  a bad idea?  Because it
    overthrows  a century  old  tradition to  let “=”  denote  a comparison  for
    equality, a predicate which is either true  or false. But Fortran made it to
    mean assignment, the  enforcing of equality. In this case,  the operands are
    on unequal footing: The left operand (a variable) is to be made equal to the
    right operand (an expression). x = y does not mean the same thing as y = x.*
	
	
	




### On retiendra
 
>En python, on peut lire `a = 1` comme  *`a` est lié à un objet de type `int` dont la valeur est 1*.
>
> On peut même préciser `a:int = 1`
>
>En Python, on copie des adresses, pas des valeurs


## Les types de base


### Les chaînes de caractère

Les chaînes de caractères sont de type  `str`. Un simple caractère est aussi une
chaîne de caractère ce qui n'est pas vrai dans d'autres langages.

Les chaînes de caractères sont représentées  par des caractères entre simples ou
doubles guillemets:

```python
>>> "Bonjour le Monde"         
 'Bonjour le Monde'

>>> 'Bonjour le Monde'         
 'Bonjour le Monde'

>>> "C'est la vie"             
 "C'est la vie"

>>> 'C'est la vie'             
  File "<ipython-input-28-11202c0f01b2>", line 1
    'C'est la vie'
         ^
SyntaxError: invalid syntax


>>> 'Il a dit "oui !"'         
 'Il a dit "oui !"'

>>> "Il a dit "oui !""         
  File "<ipython-input-30-abb082a6ed83>", line 1
    "Il a dit "oui !""
                 ^
SyntaxError: invalid syntax

>>> print('C\'est "Oui!"')     
C'est "Oui!"
```



On peut **concaténer** deux chaînes avec l'opérateur `+`:

```python
>>> 'Bonjour' + ' le Monde'    
 'Bonjour le Monde'

>>> '12' + '34'                
 '1234'

>>> '12' + ' + ' + '34'        
 '12 + 34'

>>> '12' + 34                  
------------------------------------
TypeErrorTraceback (most recent call last)
<ipython-input-36-afb1c3d28cba> in <module>
----> 1 '12' + 34

TypeError: can only concatenate str (not "int") to str
```



#### Convertir en chaîne de caractères

On effectue  souvent des  échanges avec  le monde extérieur  via des  chaînes de
caractères (import/export  de fichiers) car  c'est aussi ce que  se transmettent
les commandes du `shell` depuis les années 1970. 



On a donc  besoin de convertir des  objets en `str` (nous  verrons l'an prochain
cela plus en détail). Il suffit d'utiliser en `Python` la commande `str`:

```python
>>> str(12 + 5)                
 '17'

>>> str(12) + str(5)           
 '125'

>>> a = input('Donne-moi un entier : ')                              
Donne-moi un entier : 12

>>> a                                                                
 '12'

>>> type(a)                                                          
 str
```


### Les entiers

Les entiers de `Python`  n'ont pas de limitation en taille  si ce n'est l'espace
mémoire qu'ils occupent. 

Les opérateurs habituels sont `+, -, *, **, //, %`.

La fonction `int` a différents usages. Voyons la doc:

```python
>>> ?int                                                            
Init signature: int(self, /, *args, **kwargs)
Docstring:     
int([x]) -> integer
int(x, base=10) -> integer

Convert a number or string to an integer, or return 0 if no arguments
are given.  If x is a number, return x.__int__().  For floating point
numbers, this truncates towards zero.

If x is not a number or if base is given, then x must be a string,
bytes, or bytearray instance representing an integer literal in the
given base.  The literal can be preceded by '+' or '-' and be surrounded
by whitespace.  The base defaults to 10.  Valid bases are 0 and 2-36.
Base 0 means to interpret the base from the string as an integer literal.
>>> int('0b100', base=0)
4
```




Par exemple:

```python
>>> int('12') + int('5')                                            
 17

>>> a = int(input('Donne-moi un entier : '))                        
Donne-moi un entier : 12

>>> a + 5                                                           
 17

>>> 5 // 2                                                          
 2

>>> 5 % 2                                                           
 1

>>> int('bac', 16)                                                  
 2988

>>> int('1101', 2)                                                  
 13
```



### Les nombres à virgule flottante

Nous parlerons en détail de la représentation des nombres à virgule flottante en
machine plus  tard. Ils sont  représentés en `Python`  par le type  `float`. Les
opérateurs de base sont `+, -, *, **, /`

À noter la syntaxe pour utiliser les puissance de 10.  
Le nombre `$1,602\times 10^{-19}$` s'écrit `1.602e-19`.

Si l'on a besoin  de la racine carrée ou de toute  autre fonction élémentaire ou
même de `$\pi$`, on
peut les charger depuis la bibliothèque `math` :

```python
>>> import math
>>> math.sqrt(2)
1.4142135623730951
>>> math.pi
3.141592653589793
>>> math.sqrt(math.pi**2)
3.141592653589793
>>> math.sqrt(-1)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: math domain error
```

La  fonction `float`  va agir  un peu  comme `int`  pour convertir  un `str`  en
`float`:

```python
>>> x = float(input('Donne-moi un nombre à virgule flottante : '))
Donne-moi un nombre à virgule flottante : 3.1415926
>>> x
3.1415926
>>> x.is_integer()
False
>>> x / 2
1.5707963
```

Il  s'agit  de  conversions  *explicites*  car   on  précise  la  nature  de  la
conversion. (Définition  du dictionnaire  : *Qui est  énoncé complètement  et ne
peut prêter à aucune contestation*).


Certaines conversions  peuvent être  *implicites* (Définition du  dictionnaire :
*Qui, sans être énoncé formellement, découle naturellement de quelque chose*):

```python
>>> a = 3
>>> type(a)
<class 'int'>
>>> b = a * 2.0
>>> type(b)
<class 'float'>
>>> math.sqrt(4)
2.0
>>> type(math.sqrt(4))
<class 'float'>
```




### Les booléens


Nous avons  déjà pas mal  exploré les Booléens au  chapitre 1. En  `Python` leur
type est `bool`. Notons que c'est un  sous-type des entiers et que `True` est un
synonyme de `1` et `False` est un synonyme de `0`:

```python
>>> a = True
>>> a == 1
True
>>> 5*a
5
```

et avec un peu d'avance sur notre  programme, cela nous permettra de compter les
bonnes "réponses" à une "question":

```python
>>> def est_voyelle(c): return c in "aeiouy"

>>> texte = "Use the force young Padawan"

>>> sum(est_voyelle(c.lower()) for c in texte)
11
```

Il y a 11 voyelles dans la phrase *Use the force young Padawan*.


## Instructions et expressions conditionnelles


Une fonction  renvoyant un  booléen permet de  répondre par Vrai  ou Faux  à une
question.


```mermaid
graph LR;
A[Question]-->C{Réponse à la question};
C-->|Vraie| D[Faire/Renvoyer ceci];
C-->|Fausse| E[Faire/Renvoyer cela];
```

* Si on *fait* quelque chose, on parle d'**instruction** conditionnelle.

* Si on *renvoie* une valeur, on parle d'**expression** conditionnelle.

Voyons un calcul de maximum:

* **Instruction conditionnelle**:

```python
if x > y:
	maxi = x
else:
	maxin = y
```

* **Expression conditionnelle**:

```python
maxi = x if x > y else y
```


## `elif`

Parfois, les choix ne sont pas binaires. On doit distinguer plus de deux cas.


```python
if age < 18:
   message = "Vas te coucher !"
elif age < 60:
   message = "Tu peux regarder le film"
else :
   message = "Reveille-toi !"
```

ou

```python
message = "Vas te coucher" if age < 18 else "Tu peux regarder le film“ if age < 60 else "Reveille-toi !"
```

## Survol des structures de données 


Vous  aurez besoin  de parcourir  des « groupes »  de données  qui peuvent  avoir
différentes propriétés  selon vos  besoins. Nous  allons rapidement  parler d'un
premier lot sans  trop détailler sachant que  vous irez plus loin  si besoin une
fois en Prépa.

Nous allons surtout les découvrir telles que `Python` nous les fournit et c'est
assez spécifique.


### Listes...Python (`List`)

Les listes `Python` de type `List` ne  sont pas des listes au sens algorithmique
du terme mais pour l'instant nous ne nous en occuperons pas.

**Une  liste `Python`  est une  structure *ordonnée*  et *mutable*  et dont  les
éléments sont de type *homogène* (autant que faire se peut)...**

Les éléments d'une liste sont numérotés de  "gauche" à "droite" à partir de 0 et
de "droite" à "gauche" à partir de -1:

```
 -5 -4 -3 -2 -1  

[ a, b, c, d, e ]

 0  1   2  3  4
```

Par exemple, créons une liste de chaînes de caractères:

```python
>>> from typing import List
>>> xs: List[str] = ['Papa', 'Maman', 'La bonne', 'Moi']
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi']
>>> xs[0]
'Papa'
>>> xs[1]
'Maman'
>>> xs[2]
'La bonne'
>>> xs[3]
'Moi'
>>> xs[-1]
'Moi'
>>> xs[-2]
'La bonne'
>>> xs[-3]
'Maman'
>>> xs[-4]
'Papa'
>>> xs[-5]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>> xs[4]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
```

`len` comme *LENgth* nous permet d'avoir accès à la longueur de la liste *i.e.* le nombre de ses éléments:



```python
>>> len(xs)
4
```


On peut extraire des **sous-listes** d'une liste (ou *tranche* ou *slice*) à l'aide de la syntaxe:

```
xs[indice début : indice fin]
```

Attention ! L'indice de fin est non compris.

```python
>>> xs[1:3]
['Maman', 'La bonne']
```

Si l'on veut aller jusqu'à la fin, on n'écrit rien à droite des `:`:

```python
>>> xs[1:]
['Maman', 'La bonne', 'Moi']
```

Si l'on veut partir du début, on ne met rien à gauche des `:`:

```python
>>> xs[:3]
['Papa', 'Maman', 'La bonne']
```

Expliquez:

```python
>>> xs[1:-1]
['Maman', 'La bonne']
```

#### Concaténation - Mutabilité

On peut "coller" des listes ou plutôt les **concaténer** avec l'opérateur `+`

```python
>>> xs + ['Ma soeur']
['Papa', 'Maman', 'La bonne', 'Moi', 'Ma soeur']
```

```python
>>> ['Odin'] + xs
['Odin', 'Papa', 'Maman', 'La bonne', 'Moi']
```

Une liste Python étant *mutable*, on peut la modifier:

```python
>>> xs = ['Odin'] + xs
>>> xs
['Odin', 'Papa', 'Maman', 'La bonne', 'Moi']
>>> xs = xs[1:]
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi']
```

On peut utiliser une fonction spécifique aux listes, qui n'existe que pour les listes, et dont la syntaxe est donc différente : si on veut ajouter un élément à la fin, on utilise `append`:

```python
>>> xs.append('Ma soeur')
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi', 'Ma soeur']
```

Il y a deux choses à noter :

* `append` ne renvoie rien, ou plutôt renvoie `None` mais, "en douce" modifie la liste et lui ajoute un élément à la fin. Nous en reparlerons dans notre section sur les focntions.

* Pour appliquer `append` à la liste, on écrit `liste.append(élément)`. Nous comprendrons plus tard pourquoi : cela vient de l'orientation "Objet" de la programmation en `Python`.


#### test d'appartenance

Ce sera souvent utile:

```python
>>> xs = [1, 2, 3]
>>> 2 in xs
True
>>> 4 in xs
False
```


#### Les listes et la mémoire

Observez, c'est étrange...

```python
>>> xs = [1,2,3]
>>> id(xs)
140077907522656
>>> id(xs[0])
93990234841856
>>> ys = xs
>>> id(ys)
140077907522656
>>> xs.append(4)
>>> xs
[1, 2, 3, 4]
>>> ys
[1, 2, 3, 4]
>>> ys = ys + [5]
>>> xs
[1, 2, 3, 4]
>>> id(xs)
140077907522656
>>> id(ys)
140077906785616
>>> zs = xs[:]
>>> id(zs)
140077906700240
>>> zs
[1, 2, 3, 4]
>>> xs
[1, 2, 3, 4]
>>> ys
[1, 2, 3, 4, 5]
```

Regardez comment ça se passe sur [Pythontutor](http://pythontutor.com/visualize.html#code=xs%20%3D%20%5B1,2,3%5D%0Ays%20%3D%20xs%0Axs.append%284%29%0Ays%20%3D%20ys%20%2B%20%5B5%5D%0Azs%20%3D%20xs%5B%3A%5D&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) :

[![](./IMG/pythontutor.png)](http://pythontutor.com/iframe-embed.html#code=xs%20%3D%20%5B1,2,3%5D%0Ays%20%3D%20xs%0Axs.append%284%29%0Ays%20%3D%20ys%20%2B%20%5B5%5D%0Azs%20%3D%20xs%5B%3A%5D&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false")




### Les chaînes de caractères vues (presque) comme des listes


```python
>>> s = "Petit Papa Noël!"  

>>> len(s)                                                           
16

>>> s[:10]                                                           
'Petit Papa'

>>> s[1] = 'o'                                                      
-------------------------------------------------------------------------
TypeError                               Traceback (most recent call last)
----> 1 s[1] = 'o'

TypeError: 'str' object does not support item assignment

>>> s.split(' ')                                                    
['Petit', 'Papa', 'Noël!']

>>> ss = "abracadabrantesque"                                        

>>> ss.split('a')                                                    
['', 'br', 'c', 'd', 'br', 'ntesque']

>>> ss.split('a')                                                    
['', 'br', 'c', 'd', 'br', 'ntesque']

>>> ' '.join(["Vive", "la", "Syldavie" ]) 
'Vive la Syldavie'

>>> '$$'.join(["Vive", "la", "Syldavie" ])                          
'Vive$$la$$Syldavie'
```


### Les n-uplets (`Tuple`)


Les n-uplets (`Tuples`) de Python, c'est presque pareil, sauf que...


**Un `Tuple` `Python` est une structure *ordonnée* et *NON mutable* et dont les
éléments sont de type *homogène OU NON*...**


Donc  on favorisera  l'utilisation de  `Tuples` lorsqu'on  voudra manipuler  des
objets de types divers et qui n'ont pas vocation à évoluer.


On peut cependant faire évoluer des `Tuples` par concaténation :

```python
>>> (1,2) + (3,4)
(1, 2, 3, 4)
```

Mais  comment ajouter  un  seul élément  ?  Pour `Python`,  `(1)`  n'est pas  un
`Tuple`, c'est juste un 1 entre parenthèse :)


```python
>>> type((1))
<class 'int'>
```

Ruse ! On écrit `(1,)`

```python
>>> type((1,))
<class 'tuple'>
```

Ainsi:

```python
>>> (1,2) + (3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate tuple (not "int") to tuple
>>> (1,2) + (3,)
(1, 2, 3)
```

Bref, tout s'explique quand on cherche à comprendre !



```python

```

#### Test d'appartenance

```python
>>> t = (1, 'a', 12.3)
>>> 1 in t
True
```


### Tuple assignment

C'est souvent très pratique

```python
>>> (x, y) = (1, 3)
>>> x
1
>>> y
3
```


On  peut  même **dans  ce  cas**  se passer  de  parenthèses,  qui seront  mises
automatiquement :

```python
>>> x, y = 1, 3
>>> x
1
>>> y
3

```


Observez et expliquez :

```python
>>> x, y = 1, 2
>>> x
1
>>> y
2
>>> x, y = y, x
>>> x
2
>>> y
1
>>> x, y = y, x + y
>>> x
1
>>> y
3
```


### Les ensembles (`Set`)



**Un  **ensemble** `Python`  (`Set`) est  une structure  *NOΝ ordonnée*  et *NON
mutable* et dont les éléments sont de type *homogène OU NON* et SANS RÉPÉTITIONS...**


```python
>>> s1 = {1, 2, 3}
>>> s2 = {2, 3, 1}
>>> s3 = {1, 3, 1, 2, 2, 3}
>>> s1 == s2
True
>>> s1 == s3
True
>>> s1
{1, 2, 3}
>>> s2
{1, 2, 3}
>>> s3
{1, 2, 3}
```

#### Opérations

On a l'union : `|`

```python
>>> s1 = {1, 2, 3}
>>> s2 = {2, 3, 4, 5}
>>> s1 | s2
{1, 2, 3, 4, 5}
```

On a l'intersection

```python
>>> s1 & s2
{2, 3}
```

On a l'appartenance

```python
>>> 2 in {1, 2, 3}
True
```

et l'inclusion:

```python
>>> {1, 2} < {1, 2, 3}
True
>>> {1, 2} <= {1, 2}
True
>>> {1, 2} < {1, 2}
False
```

et la différence (on enlève les éléments du second dans le premier):

```python
>>> {1, 2, 3} - {2, 3, 4}
{1}
>>> {2, 3, 4} - {1, 2, 3}
{4}
```


### `range`

C'est un objet un peu à part...Voyons l'aide :


```console
Help on class range in module builtins:

class range(object)
 |  range(stop) -> range object
 |  range(start, stop[, step]) -> range object
 |  
 |  Return an object that produces a sequence of integers from start (inc
lusive)
 |  to stop (exclusive) by step.  range(i, j) produces i, i+1, i+2, ..., 
j-1.
 |  start defaults to 0, and stop is omitted!  range(4) produces 0, 1, 2,
 3.
 |  These are exactly the valid indices for a list of 4 elements.
 |  When step is given, it specifies the increment (or decrement).
 |  
```

```python
>>> r = range(1, 5)
>>> 1 in r
True
>>> 0 in r
False
>>> 4 in r
True
>>> 5 in r
False
>>> 2.5 in r
False
```


## Les Boucles


### Boucles conditionnelles

*L'humain pense, la machine répète*

Supposons que l'on veuille afficher une série de "Bonjour" numérotés :

```python
"C'est mon bonjour numéro 1"
"C'est mon bonjour numéro 2"
"C'est mon bonjour numéro 3"
"C'est mon bonjour numéro 4"
```

On écrit :

```python
no = 1                                                          

while no < 5: 
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
```

La syntaxe est :

```
while <expression booléenne>:
	faire ceci
	faire cela
faire autre chose quand l'expression booléenne est devenue fausse
```


En fait, cela revient écrire :

```python
no = 1

if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
if no < 5:
    print( f"C'est mon bonjour numéro {no}" ) 
    no = no + 1 
	
....
```

mais qui s'arrêterait lorsque l'expression booléenne devient fausse.


#### Incrément

L'expression `no = no  + 1` est bien sûr une affectation et  non pas une égalité
mathématique qui serait absurde.

Comme dans la  plupart des autres langages qui  permettent l'affectation, Python
dispose  d'un *sucre  syntaxique*: `no  +=  1` qui  se décline  avec tout  autre
opérateur `n -= 1`, `n *= 2`, `n /= 2`,...
	

Par exemple **quelle est la première puissance de 2 qui dépasse 10000 ?**


```python
In [39]: n = 1

In [40]: while n < 10000:
    ...:     n *= 2

In [41]: n
Out[41]: 16384

In [42]: n = 1

In [43]: while n < 10000: 
    ...:     n <<= 1

In [44]: n
Out[44]: 16384
```



### Boucles *foreach* : pour chaque élément d'une collection

On peut aussi prendre chaque élément d'une collection et faire quelque
chose avec ces éléments.

Par exemple, on dispose d'une liste d'élèves et on veut leur dire bonjour:

```python
In [49]: classe = {'Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza'}          

In [50]: for élève in classe: 
    ...:     print(f"Bonjour {élève}") 
    ...:                                                                 
Bonjour Isa
Bonjour Bill
Bonjour Joe
Bonjour Max
Bonjour Zoé
Bonjour Zaza
```

Si l'on veut un peu d'ordre:

```python
In [53]: classe = ['Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza']           

In [54]: for élève in classe: 
    ...:     print(f"Bonjour {élève}") 
    ...:                                                                 
Bonjour Joe
Bonjour Max
Bonjour Bill
Bonjour Zoé
Bonjour Isa
Bonjour Zaza
```

La syntaxe est donc :

```python
for élément in collection: # pour chaque élément dans la collection
    faire ceci
faire cela # quand on a fini de parcourir TOUS les éléments de la collection
```

### Cas particulier du parcours d'un "range"

Si l'on veut parcourir une  collection de nombres entiers espacés régulièrement,
on peut utiliser  `for indice in range(début,  fin + 1, pas[qui  vaut par défaut
1])`.


Par exemple si on veut sélectionner un  élève sur deux dans la classe en partant
du premier:


```python
In [1]: classe = ['Joe', 'Max', 'Bill', 'Zoé', 'Isa', 'Zaza']                                                                                        

In [2]: for indice in range(0, len(classe), 2): 
   ...:     print(classe[indice]) 
   ...:                                                                                                                                              
Joe
Bill
Isa
```

### DÉFI

On dispose d'une liste `fi = [1, 1, 0, 0, 0, ..., 0]` de longueur 20.

On veut remplacer  tous les termes à  partir du troisième par la  somme des deux
précédents. 

On veut donc obtenir `[1, 1, 2, 3, 5, 8, 13,...]`

Comment faire ?



### Le beurre et l'argent du beurre : obtenir la valeur d'un élément et son rang
### dans une liste 


Comment  mettre  en  majuscule les  lettres  de  rang  pair  d'un mot  écrit  en
minuscules.

On utilisera  la méthode  `upper()` qui  transforme une  lettre en  la majuscule
correspondant:

```python
In [3]: 'a'.upper()                                                                                                                                  
Out[3]: 'A'

In [4]: 'A'.upper()                                                                                                                                  
Out[4]: 'A'

In [5]: '1'.upper()                                                                                                                                  
Out[5]: '1'

In [6]: 'Papa'.upper()                                                                                                                               
Out[6]: 'PAPA'
```


Et  surtout  `enumerate(collection)`  qui  renvoie  la  l'ensemble  des  couples
`(rang, valeur)` pour chaque valeur de la collection.

```python
In [14]: mot = input('Rentrer un mot : ')
Rentrer un mot : anticonstitutionnellement

In [15]: new_mot = ''

In [16]: for rang, lettre in enumerate(mot): 
    ...:     new_mot += lettre.upper() if rang % 2 == 0 else lettre 

In [17]: new_mot
Out[17]: 'AnTiCoNsTiTuTiOnNeLlEmEnT'
```


### Définitions par compréhension


On dispose d'une liste de lettres en minuscules et on veut créer une autre liste
des mêmes lettres en majuscules :

```python
In [21]: ls = ['a', 'b', 'c', 'd']

In [22]: [lettre.upper() for lettre in ls]
Out[22]: ['A', 'B', 'C', 'D']
```

On utilise la construction par compréhension :

```
{ f(x) for x in collection }
# l'ensemble des f(x) pour chaque élément x de la collection
```



#### Filtre

On peut utiliser un filtre en  rajoutant une expression booléenne introduite par
`if`. Par exemple, une liste de nombre  étant donnée, on veut fabriquer la liste
des multiples communs de 8 et 6 de cette liste :

```python
>>> [nb for nb in range(200) if nb % 6 == 0 and nb % 8 == 0]
[0, 24, 48, 72, 96, 120, 144, 168, 192]
```














## Fonctions

Vous  allez   être  amenés  à  construire   des  programmes  de  plus   en  plus
élaborés.  Vous  allez   avoir  besoin  de  les  organiser   en  morceaux  plus
petits. Cela sera plus facile à  contrôler, mais vous permettra aussi d'utiliser
ces blocs élémentaires pour d'autres programmes : c'est la **modularité**.


Une fonction est **déf**inie sur Python en l'introduisant avec le mot-clé `def`.

On **appelle** ensuite la fonction...par son nom.

### Fonction : premier contact


**Déf**inissons par exemple  une fonction qui affiche des  données personnelles sur
une personne :

```python
def afficheMesDonnees():
    print("James")
    print("Bond")
    print("jbond@secret.uk")
```

À présent **appel**ons cette fonction:

```console
>>> afficheMesDonnees()
James
Bond
jbond@secret.uk
```
Une fonction doit être appelée ! 

Quel est l'intérêt ? On aurait pu tout aussi bien écrire :


```python
print("James")
print("Bond")
print("jbond@secret.uk")
```

Oui mais, si  nous avons besoin d'écrire souvent et  dans différents endroits du
code ces adresses, cela  nous permet de ne pas tout réécrire et  en plus, pour la
lecture du code, cela traduit une action.





### Deuxième contact : paramètre et argument


Imaginons  que toute  la famille  Bond  a le  même type  d'adresse e-mail,  seule
l'initiale du prénom changeant. On peut alors proposer le code suivant :


```python
def afficheLesDonneesDe(prenom):
    print(prenom)
    print("Bond")
    print(f"{prenom[0].lower()}bond@secret.uk")
```

et cette fois :

```console
>>> afficheLesDonneesDe("Max")
Max
Bond
mbond@secret.uk

>>> afficheLesDonneesDe("Barbara")
Barbara
Bond
bbond@secret.uk
```

`afficheLesDonneesDe("Max")`  est  le  code **appelant**  (*caller*).  Il  **passe**
l'**argument** (ou **paramètre réel) `Max` à la fonction **appelée** (...par l'appelant).

Quand elle  reçoit l'argument  `"Max"`, la  fonction substitue  le **paramètre**
(ou **paramètre formel**) `prenom` par l'argument `"Max"`.

* un paramètre (formel) est une variable **non-liée** (*unbound*) ;
* un argument (ou paramètre réel) peut être une valeur, une variable liée ou une expression
  complexe.
  
Ainsi, les paramètres ne changent pas d'un  appel à l'autre de la fonction alors
que  les arguments  peuvent changer  (en fait,  ça peut  être plus  compliqué et
subtil mais on se tiendra à cette explication à notre niveau...).


Dans la  pratique, on  peut parfois utiliser  un terme pour  un autre  et parler
d'argument au lieu de paramètre, ou d'argument formel au lieu de paramètre. Bon,
le principal est de bien voir la différence entre les deux notions et on essaiera
donc d'utiliser correctement paramètre et argument.


## Troisième contact : plusieurs arguments

Et que se passe-t-il  si nous voulons aussi changer le nom de  famille et le nom
de domaine ? Il faut passer plusieurs arguments et donc avoir une fonction ayant
plusieurs paramètres...


```python
def afficheLesDonneesDe(prenom, nom, domaine):
    print(prenom)
    print(nom)
    print(f"{prenom[0].lower()}{nom.lower()}@{domaine}")

afficheLesDonneesDe('James', 'Haskell', 'wasps.uk')
```

et on obtient:

```console
$ python fonction_cours.py
James
Haskell
jhaskell@wasps.uk
```

### Quatrième contact : le retour

Tout ceci ne  nous convient pas pour  plusieurs raisons : nous  avions comme mot
d'ordre  de n'avoir  qu'un seul  `print` par  programme pour  matérialiser notre
sortie  finale.  Il faut  donc  abolir  les `print`  et  autres  sorties de  nos
fonctions.

Il faut de plus avoir en tête  qu'une fonction est une machine à transformer des
paramètres d'entrée en paramètres de sortie :

```mermaid
graph LR;
	A{Entrée}-->B[Fonction];
	B-->C{Sortie};
```

Nous préférerons donc la forme suivante qui utilise le mot-clé `return` :

```python
def lesDonneesDe(prenom, nom, domaine):
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees

print(lesDonneesDe('James', 'Haskell', 'wasps.uk'))
```

Nous pouvons même typer nos fonctions pour faciliter la lecture des programmes :

```python
def lesDonneesDe(prenom: str, nom: str, domaine: str) -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}\n"
    return donnees
	
print(afficheLesDonneesDe('James', 'Haskell', 'wasps.uk'))
```

De manière générale, il faudra présenter les fonctions sous cette forme:


```python
def nomDeLaFonction(param1: type1, param2: type2, ...) -> type_de_la_sortie:
    Traitement des paramètres 
	return valeur_de_sortie
```
 Pour être complet et  si la fonction n'est pas évidente, il  faut prendre l'habitude de
 la documenter à l'aide d'un *docstring*  dans lequel on fera figurer une rapide
 explication  et quelques  tests. Les  *docstrings* sont  entre trois  paires de
 guillemets.
 
 La fonction `afficheLesDonneesDe`  a un nom suffisemment  clair, les paramètres
 sont typés et portent des noms explicites. Un *docstring* serait redondant.


### Cinquième contact : réutilisation

Supposons que nous  disposions d'une liste de triplets  `(prenom, nom, domaine)`
et que nous voulions afficher les données comme d'habitude.

On dispose donc d'une liste du type : 


```python
data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk"), ("Mata", "Hari", "spion.de"), ("Sofia", "Loren", "cinecitta.it")]
```

Plusieurs possibilités s'offrent à nous.  Nous pouvons utiliser la fonction déjà
définie mais faire attention que l'on récupère des triplets quand on parcourt la
liste alors que la fonction attend trois paramètres.

On peut faire :

```python
sortie = ""
for triplet in data:
    prenom, nom, domaine = triplet
    sortie += lesDonneesDe(prenom, nom, domaine)

print(sortie)
```

On peut  créer une fonction  qui traite les listes  de données. Pour  suivre nos
instructions, il  faut pouvoir indiquer que  le paramètre de cette  fonction est
une liste  de triplets  de chaînes  de caractères. Nous  avons besoin  pour cela
d'importer la  bibliothèque `typing` qui  nous permet d'utiliser des  types plus
complexes que les simples `str`, `int`,...
Ici, on va devoir créer des listes  contenant des triplets de chaînes. Ce seront
des `List[Tuple[str, str, str]]`


```python
from typing import List, Tuple

def tasDeDonnees(donnees: List[Tuple[str, str, str]]) -> str:
    """
    donnees est une liste de triplets de la forme (prenom, nom, domaine)
    revoie une chaîne sous la forme :
    prenom
    nom
    pnom@domaine

    >>> data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk")]
    >>> tasDeDonnees(data)
    'James\nHaskell\njhaskell@wasps.uk\nJames\nBond\njbond@secret.uk\n'
    """
    sortie = ""
    for triplet in donnees:
        prenom, nom, domaine = triplet
        sortie += lesDonneesDe(prenom, nom, domaine)
    return sortie

print(tasDeDonnees(data))
```

### Sixième contact : fonctions pures et impures

Une  fonction doit  toujours renvoyer  quelque chose,  même si  cette chose  est
`None`.

Voyons `print` par exemple.

```console
In [6]: type(print(2))
2
Out[6]: NoneType

In [7]: print(2)
2

In [8]: [print(k) for k in range(4)]
0
1
2
3
Out[8]: [None, None, None, None]
```

en fait, `print` est une fonction qui  renvoie `None`. MAIS elle ne fait pas que
ça. Si nous lisons sa documentation:

```console
In [5]: ?print
Docstring:
print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)

Prints the values to a stream, or to sys.stdout by default.
Optional keyword arguments:
file:  a file-like object (stream); defaults to the current sys.stdout.
sep:   string inserted between values, default a space.
end:   string appended after the last value, default a newline.
flush: whether to forcibly flush the stream.
Type:      builtin_function_or_method
```

Elle  a donc  un **effet  secondaire** (*side  effect*) en  dehors de  la valeur
renvoyée. Ce  n'est donc pas  une fonction pure car  elle fait quelque  chose en
dehors de sa signature.

Il faut,  si possible, éviter ce  genre de fonctions car  les effets secondaires
sont  sources  de  nombreux  bugs.   Imaginons  par  exemple  une  variable  qui
représente le score à un jeu durant une partie : 

```python
score: int = 1

def new_score(points: int) -> int:
    global score
    score = score + points
    return score

for parties in range(1,5):
    print(f"\nJ'ai gagné 10 points ! new_score(10) = {new_score(10)}")
```

Ce qui peut préter confusion :



```console
J'ai gagné 10 points ! new_score(10) = 11

J'ai gagné 10 points ! new_score(10) = 21

J'ai gagné 10 points ! new_score(10) = 31

J'ai gagné 10 points ! new_score(10) = 41
```


On remarque que les mêmes appels à `new_score(10)` ne renvoient pas la même
valeur. La variable `score`  est *globale* : elle est en effet  définie en dehors de
la  fonction.  Mais elle  est  modifiée  par cette  fonction  :  c'est un  effet
secondaire de l'appel de cette fonction.

>Une fonction pure est une fonction qui,  quand on lui passe les mêmes arguments,
>renverra toujours la même valeur.
>
>Une fonction impure va changer l'état du système

Il est donc très difficile de contrôler ce genre de fonctions. On peut être
tenté  de les  utiliser  lorsqu'on veut  faire  évoluer un  plateau  de jeu  par
exemple. Mais il faut être conscient des risques.



### Septième contact : paramètre par défaut

Certains  paramètres  d'une fonction  peuvent  avoir  le  plus souvent  la  même
valeur. On leur donne cette valeur par défaut dans la signature de la fonction.

Par  exemple,  reprenons la  fonction  `lesDonneesDe`  mais supposons  que  nous
travaillions pour une entreprise dont tous  les employés ont pour nom de domaine
`mafirme.fr`. Nous  pouvons nous éviter  de toujours  rentrer le nom  de domaine
pour gagner du temps:

```python
def lesDonneesDe(prenom: str, nom: str, domaine: str = "mafirme.fr") -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees
```

Alors on obtient :

```python
>>> print(lesDonneesDe("Ada", "Lovelace"))
Ada
Lovelace
alovelace@mafirme.fr

>>> print(lesDonneesDe("Charles", "Babbage", "machine.en"))
Charles
Babbage
cbabbage@machine.en
```






### Bilan du vocabulaire

```python
def test(x: int, y: int) -> int:
    somme = 0
    for i in range(y):
        somme = somme + x
    return somme

z = test(5, 4)
```

Dans le script ci dessus :

1. Quel est le nom de la fonction ?
2. Quels sont les paramètres (formels) de la fonction ?
3. Quels sont les arguments (ou paramètres réels) ?
4. Quelles sont les variables locales à la fonction ?
5. Quelle sera la valeur de `z` après l'exécution de ce script ?
6. Quelle est la signature de la fonction ?
7. Est-elle pure ?

### Correction

1. `test`
2. deux paramètres formels : `x` et `y`
3. paramètres réels sont 5 et 4
4. variables locales à la fonction : `somme` et `i`
5. après l'exécution de ce script, `z` vaudra 20.
6. `(x: int, y: int) -> int`
7. Oui : pas d'effets secondaires.


### Structure d'un programme

Un programme va maintenant se présenter en trois parties:

1. Une série d'importations de bibliothèques utilisées en 2.
2. Une série de fonctions qui seront utiles en 3.
3. Un programme  principal qui appellera judicieusement des  fonctions créées en
   2.
   
   
Par exemple ;

```python
#1 Les importations
from typing import List, Tuple

#2 Les fonctions
def lesDonneesDe(prenom: str, nom: str, domaine: str) -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}\n"
    return donnees

def tasDeDonnees(donnees: List[Tuple[str, str, str]]) -> str:
    """
    donnees est une liste de triplets de la forme (prenom, nom, domaine)
    revoie une chaîne sous la forme :
    prenom
    nom
    pnom@domaine

    >>> data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk")]
    >>> tasDeDonnees(data)
    'James\nHaskell\njhaskell@wasps.uk\nJames\nBond\njbond@secret.uk\n'
    """
    sortie = ""
    for triplet in donnees:
        prenom, nom, domaine = triplet
        sortie += lesDonneesDe(prenom, nom, domaine)
    return sortie

#3 Le programme principal
## L'entrée
data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk"), ("Mata", "Hari", "spion.de"), ("Sofia", "Loren", "cinecitta.it")]

## Le traitement
res = tasDeDonnees(data)

## La sortie
print(res)
```


