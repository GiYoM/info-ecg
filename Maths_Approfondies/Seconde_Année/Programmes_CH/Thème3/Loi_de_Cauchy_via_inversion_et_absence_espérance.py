import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd



# Méthode d'inversion - loi de Cauchy
a=2
N=10000
U=rd.random(N)

plt.figure(0)

b=np.arange(-50,50,0.2)
x=np.arange(-10,10,0.1)
X=a*np.tan(np.pi*(U-1/2))


plt.subplot(2,1,1)
plt.hist(X,density='true',bins=b)
plt.plot(x,a/np.pi/(a**2+x**2),'r')
axes = plt.gca() 
plt.axis([-16,16,0,0.2])

plt.subplot(2,1,2)
plt.hist(X, bins=b,cumulative = True,histtype = 'step',density='true')
plt.plot(x,1/2+np.arctan(x/a)/np.pi,'b')
axes = plt.gca() 
plt.axis([-8,8,0,1])


# Non existence de l'espérance
# Je suis preneuse d'autres façons d'illustrer ça :-)
plt.figure(1)
n=1000

m=[np.average(a*np.tan(np.pi*(rd.random()-1/2))) for i in range(n)]
plt.plot(range(n),m)
   

plt.show()