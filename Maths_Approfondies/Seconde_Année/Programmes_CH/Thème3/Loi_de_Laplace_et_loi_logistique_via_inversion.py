import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd


# Méthode d'inversion - variables à densité, loi de Laplace et loi logistique standards
# Ouverture d'une nouvelle fenêtre pour chaque question


def G(u):
    y=np.log(2*u)*(u<1/2)-np.log(2*(1-u))*(u>=1/2)
    return(y)
    
    
N=10000
U=rd.random(N)


# loi de Laplace standard
plt.figure(0)

X=G(U)
 

plt.subplot(2,1,1)
x=np.arange(-5,5,0.1)
plt.hist(X,density='true',bins=100)
plt.plot(x,np.exp(-abs(x))/2,'r')

plt.subplot(2,1,2)
plt.hist(X, bins=x,cumulative = True,histtype = 'step',density='true')
plt.plot(x,np.exp(x)/2*(x<0)+(1-np.exp(-x)/2)*(x>=0),'b')



    

# loi logistique standard
plt.figure(1)

X=np.log(U/(1-U))
 

plt.subplot(2,1,1)
x=np.arange(-5,5,0.1)
plt.hist(X,density='true',bins=100)
plt.plot(x,np.exp(-x)/(1+np.exp(-x))**2,'r')

plt.subplot(2,1,2)
plt.hist(X, bins=x,cumulative = True,histtype = 'step',density='true')
plt.plot(x,1/(1+np.exp(-x)),'b')


plt.show()
