import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd



N=10000
lam=3
U=rd.random(N)


# Loi exponentielle via la méthode d'inversion
plt.figure(0)

X=-1/lam*np.log(U)

plt.subplot(2,1,1)
x=np.arange(0,2,0.01)
plt.hist(X,density='true',bins=x)
plt.plot(x,np.exp(-lam*x)*lam,'b')

plt.subplot(2,1,2)
plt.hist(X, bins=x,cumulative = True,histtype = 'step',density='true')
plt.plot(x,1-np.exp(-lam*x),'b')

# loi géométrique à partir d'une exponentielle
plt.figure(1)
p=0.2


plt.subplot(3,1,1)

Xg=np.floor(np.log(1-U)/np.log(1-p))+1
   
nmax=np.int(np.max(Xg))
b=np.arange(0.5,nmax+1.5,1)

plt.hist(Xg,bins=b,density='true',rwidth = 0.08)


axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])



plt.subplot(3,1,2)

Xg=rd.geometric(p,(N))



plt.hist(Xg,bins=b,density='true',rwidth = 0.08)

axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])



plt.subplot(3,1,3)
loig=np.zeros(nmax)
loig[0]=p
for k in range(1,nmax):
    loig[k]=loig[k-1]*(1-p)

for i in range(nmax):
    plt.plot([i+1,i+1],[0,loig[i]],'r')
#plt.bar(range(1,nmax+1),loig,width=0.08)
axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])



plt.show()