import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import scipy.special as sp


# Les 12 uniformes
N=10000;
x=np.arange(-5,5,0.1)


def phi(x):
    return(np.exp(-(x**2)/2)/np.sqrt(2*np.pi))

def Phi(x):
    return(sp.ndtr(x))

U=rd.random([N,12])

X1=np.sum(U,1)-6
X2=rd.normal(0,1,N);

plt.subplot(2,2,1);
plt.plot(x,phi(x))
plt.hist(X1,density='true',bins=100)

plt.subplot(2,2,3);
plt.plot(x,phi(x))
plt.hist(X2,density='true',bins=100)

plt.subplot(2,2,2);
plt.plot(x,Phi(x))
plt.hist(X1, bins=x,cumulative = True,histtype = 'step',density='true')

plt.subplot(2,2,4);
plt.plot(x,Phi(x))
plt.hist(X2, bins=x,cumulative = True,histtype = 'step',density='true')
   

plt.show()