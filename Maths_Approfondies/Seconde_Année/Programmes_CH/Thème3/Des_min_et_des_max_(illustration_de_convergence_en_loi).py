import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd

# Deux façons de faire (min vs max) : je ne sais pas s'il y a une version à privilégier, ou s'il y a mieux/plus simple ?

N=10000
c=['blue', 'green', 'yellow', 'red']
Xmin=np.zeros((N,4))
k=0
plt.subplot(2,2,1)
for n in [2,5,10,50]:
    for i in range(N):
        Xmin[i,k]=np.min(-1+2*rd.random(n))
    plt.hist(Xmin[:,k],density='true', bins=20,color=c[k])
    k=k+1

plt.subplot(2,2,2)
for k in range(4):
    L=len(Xmin[:,k])
    Y = np.sort(Xmin[:,k])
    Z = np.arange(1,L+1)/L
    plt.step(Y,Z,color=c[k])
    

k=0
Xmax=[]
plt.subplot(2,2,3)
for n in [2,5,10,50]:
    Xmax+=[np.max(-1+2*rd.random((N,n)),1)]
    plt.hist(Xmax[k],density='true',bins=20,color=c[k])
    k=k+1

plt.subplot(2,2,4)
for k in range(4):
    plt.hist(Xmax[k], bins=L,cumulative = True,histtype = 'step')
  



plt.show()
