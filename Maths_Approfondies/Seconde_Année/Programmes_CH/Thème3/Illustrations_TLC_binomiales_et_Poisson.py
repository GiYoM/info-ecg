import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import scipy.special as sp

# Illustrations du TLC


N=10000
x=np.arange(-5,5,0.1)


def phi(x):
    return(np.exp(-(x**2)/2)/np.sqrt(2*np.pi))

def Phi(x):
    return(sp.ndtr(x))


# Lois binomiales
plt.figure(0)

i=1
c=['b','g','r','b','g','r','b','g','r']
for p in [0.1,0.5,0.9]:
    for n in [10,100,1000]:
        X=(rd.binomial(n,p,N)-n*p)/np.sqrt(n*p*(1-p))
        b=(np.arange(-1,n+1)-n*p+1/2)/np.sqrt(n*p*(1-p))
        plt.subplot(3,3,i)
        plt.plot(x,phi(x))
        plt.hist(X,bins=b,density='true',color=c[i-1])
        plt.title('p='+str(p)+' et n='+str(n))
        axes = plt.gca() 
        plt.axis([-5,5,0,1/np.sqrt(2*np.pi)])
        i+=1
plt.tight_layout(pad=0, w_pad=0, h_pad=1.0)
   
  
  
# Lois de Poisson
plt.figure(1)


i=1
c=['b','g','r','y','m','c']
for lam in [1,5,10,50,100,1000]:
    X=(rd.poisson(lam,N)-lam)/np.sqrt(lam);
    b=(np.arange(-1,lam+3*np.sqrt(lam),1)-lam+1/2)/np.sqrt(lam);
    plt.subplot(3,3,i)
    plt.plot(x,phi(x))
    plt.hist(X,bins=b,density='true',color=c[i-1])
    plt.title('lambda='+str(lam))
    axes = plt.gca() 
    plt.axis([-5,5,0,1/np.sqrt(2*np.pi)])
    i+=1
plt.tight_layout(pad=0, w_pad=0, h_pad=1.0)

plt.show()