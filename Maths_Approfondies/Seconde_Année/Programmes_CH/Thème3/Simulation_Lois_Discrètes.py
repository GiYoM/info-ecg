import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd

# Simulation des lois binomiales, de Poisson et géométrique, en ouvrant à chaque fois une fenêtre différente

N=10000

plt.figure(0)
n=10;p=0.7
b=np.arange(0.5,n+1.5,1)



plt.subplot(3,1,1)

#Xb=np.zeros(N)
#for k in range(N):
#    Xb[k]=0
#    for i in range(n):
#        if rd.random()<p:
#            Xb[k]=Xb[k]+1

Xb=np.sum(rd.random((N,n))<p,1)
plt.hist(Xb,bins=b,density='true',rwidth = 0.08)
axes = plt.gca() 
plt.axis([0, n, 0, 0.3])


plt.subplot(3,1,2)

Xb=rd.binomial(n,p,N)
plt.hist(Xb,bins=b,density='true',rwidth = 0.08)
axes = plt.gca() 
plt.axis([0, n, 0, 0.3])

plt.subplot(3,1,3)

def binom(k,n):
    if k>n/2:
        k=n-k
    p=1
    for i in range(1,k+1):
        p*=(n-i+1)/i
    return(p)
        


loi=np.zeros(n+1)
for k in range(n+1):
    loi[k]=binom(k,n)*p**k*(1-p)**(n-k)
    plt.plot([k,k],[0,loi[k]],'black') # Est-ce qu'il y a plus simple pour faire ça ?
    #personnellement (c'est Marielle qui parle), j'ai choisi d'utiliser plt.bar qui permet de faire un diagramme en bâtons ...


axes = plt.gca() 
plt.axis([0, n, 0, 0.3])


plt.figure(1)
lam=3

plt.subplot(2,1,1)
Xp=rd.poisson(lam,(N))
nmax=np.max(Xp)
b=np.arange(-0.5,nmax+1.5,1)

plt.hist(Xp,bins=b,density='true',rwidth = 0.08)


axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])

plt.subplot(2,1,2)

loip=np.zeros(nmax+1)
loip[0]=np.exp(-lam)
for k in range(0,nmax):
    loip[k+1]=loip[k]*lam/(k+1)
    plt.plot([k,k],[0,loip[k]],'black')

axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])

plt.figure(2)


p=0.2

plt.subplot(3,1,1)

Xg=np.zeros(N)
for i in range(N):
    Xg[i]=1
    while rd.random()>p:
        Xg[i]+=1
   
nmax=np.int(np.max(Xg))

b=np.arange(0.5,nmax+1.5,1)
plt.hist(Xg,bins=b,density='true',rwidth = 0.08)

axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])



plt.subplot(3,1,2)

Xg=rd.geometric(p,(N))
plt.hist(Xg,bins=b,density='true',rwidth = 0.08)

axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])



plt.subplot(3,1,3)
loig=np.zeros(nmax+1)
loig[0]=p
for k in range(1,nmax+1):
    loig[k]=loig[k-1]*(1-p)
    plt.plot([k,k],[0,loig[k-1]],'black')



axes = plt.gca() 
plt.axis([0, nmax, 0, 0.25])


plt.show()
