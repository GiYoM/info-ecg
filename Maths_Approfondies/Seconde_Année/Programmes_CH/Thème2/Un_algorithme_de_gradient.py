import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
from matplotlib import cm

# Un algorithme de gradient

Rx=3;Ry=3
dx=0.01;dy=0.01
x=np.arange(-Rx,Rx,dx) ; y=np.arange(-Ry,Ry,dy)
x, y = np.meshgrid(x,y)


def nabla_f(v):
    [x,y]=v
    grad = [(3*x**2-2*x*(x**3+y**2))*np.exp(-(x**2+y**2)),2*y*(1-(x**3+y**2))*np.exp(-(x**2+y**2))]
    return(np.array(grad))



def f(x,y):
    return((x**3+y**2)*np.exp(-(x**2+y**2)))
    

plt.contour(x,y,f(x,y),30, cmap=cm.jet)

eps = 10**(-6) ; lam = 0.5
max_iter = 200; 
difference = 1
iter = 0 ; 
v = np.array([2*Rx*(rd.random()-0.5),2*Ry*(rd.random()-0.5)]) 


while  (difference > eps) and (iter < max_iter): 
    iter = iter + 1
    v_old = v
    v = v- lam*nabla_f(v)
    difference =np.sqrt(sum((v-v_old)**2))
    plt.scatter(v[0],v[1],marker="+",c="black")

  
if iter==max_iter:
    print('Pas de convergence')
else:
    print('Convergence en ',iter,' iterations vers le point (',v[0],',',v[1],')')

plt.show()