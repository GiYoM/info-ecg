import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

Rx=3;Ry=3
dx=0.01;dy=0.01
x=np.arange(-Rx,Rx,dx) ; y=np.arange(-Ry,Ry,dy)



def f(x,y):
    return((x**3+y**2)*np.exp(-(x**2+y**2)))

def nabla_1(x,y):
    return((3*x**2-2*x*(x**3+y**2))*np.exp(-(x**2+y**2)))
def nabla_2(x,y):
    return(2*y*(1-(x**3+y**2))*np.exp(-(x**2+y**2)))





plt.figure(0) # Graphe 3D

x, y = np.meshgrid(x,y)
ax = plt.axes(projection = '3d')
ax.plot_surface(x, y, f(x,y), cmap=cm.jet)



plt.figure(1) # Lignes de niveau sur le graphe 3D


ax = plt.axes(projection='3d')
ax.contour3D(x, y, f(x,y), 50, cmap=cm.jet)


plt.figure(2) # Lignes de niveau 2D


graphe=plt.contour(x,y,f(x,y),20, cmap=cm.jet) #20 lignes de niveau


#graphe=plt.contourf(x,y,f(x,y),20, cmap=cm.jet) # version "on remplit entre les lignes"

#plt.clabel(graphe,inline=1,fontsize=10) pour indiquer les niveaux

plt.figure(3) # Tracé du champ de gradient


plt.contour(x,y,f(x,y),50, cmap=cm.jet)

xb=np.arange(-Rx,Rx,0.2) ; yb=np.arange(-Ry,Ry,0.2)
x, y = np.meshgrid(xb,yb)

plt.quiver(x, y, nabla_1(x,y), nabla_2(x,y),units='xy' ,scale=2, cmap=cm.jet)



plt.show()