import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd


N=100
E=rd.exponential(3,(N,2))
D=np.floor(10*E)/10
F=E+3*rd.random((N,2))



A=[E[:,0],E[:,0],E[:,0]]
B=[E[:,1],D[:,0],F[:,0]]


for i in range(3):
    plt.subplot(3,1,i+1)
    
    X=A[i]
    Y=B[i]
    plt.scatter(X,Y)


    mx=np.mean(X)
    my=np.mean(Y)
    vx=np.var(X)
    vy=np.var(Y)

    cov_xy=np.dot(X,Y)/N-mx*my # on est d'accord que np.cov n'est pas au programme ?
    r_xy=cov_xy/np.sqrt(vx*vy)

    print("Coefficient de corrélation linéaire",i,"):",r_xy)

    a=cov_xy/vx
    b=my-a*mx
    aprime=cov_xy/vy
    bprime=mx-aprime*my
    x=np.arange(min(X),max(X),1)


    plt.plot(x,a*x+b,'r')
    plt.plot(x,1/aprime*(x-bprime),'b')

plt.show()