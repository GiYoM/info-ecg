import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd


p=0.45;N=1000;n=100
X=0+(rd.random((N,n))<p)
U=[np.average(X[i,:]) for i in range(N)]


k=0
for i in range(N):
    if np.abs(U[i]-p)<=1.96/(2*np.sqrt(n)):
        plt.plot([i+1, i+1],[U[i]-1.96/(2*np.sqrt(n)), U[i]+1.96/(2*np.sqrt(n))],"g")
    else:
        plt.plot([i,i],[U[i]-1.96/(2*np.sqrt(n)), U[i]+1.96/(2*np.sqrt(n))],"r")
        k=k+1


plt.plot([0 ,N+1],[p, p],'black')
axes = plt.gca() 
plt.axis([0,N,np.min(U-1.96/(2*np.sqrt(n))-0.01),np.max(U+1.96/(2*np.sqrt(n))+0.01)])

print('Nombre de sorties de route : ',k)

plt.show()
