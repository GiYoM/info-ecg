import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd

a=(5*np.log(10)+np.log(2))

for n in [100,1000,10000]:
    print('n = ',n)
    U = a*rd.random(n)
    X=2*a*np.exp(-U**2)
    print("Intervalle à 95% avec variance empirique : [",np.average(X)-1.96*np.std(X)/np.sqrt(n),",",np.average(X)+1.96*np.std(X)/np.sqrt(n),"]")
    print("Erreur: ", np.abs(np.average(X)-np.sqrt(np.pi)))
 



plt.show()