import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd

p=0.2
N=1000
U=(rd.random(N)<p)+0


plt.plot([1, N],[p, p],'black')
for n in np.arange(100,N,100):
    M_emp=np.average(U[1:n])
    plt.plot([n,n],[M_emp-np.sqrt(5)/np.sqrt(n),M_emp+np.sqrt(5)/np.sqrt(n)],'b')
    plt.plot([n,n],[M_emp-1.96/2/np.sqrt(n),M_emp+1.96/2/np.sqrt(n)],'r')  


plt.show()