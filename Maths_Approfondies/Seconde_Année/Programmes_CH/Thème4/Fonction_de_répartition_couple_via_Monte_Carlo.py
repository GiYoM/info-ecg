import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import scipy.special as sp
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

# Fonction de répartition d'un couple de var (indépendantes pour pouvoir vérifier)

def F_X(x):
    return(np.average(X<=x))

F_X=np.vectorize(F_X)

def F_Y(x):
    return(np.average(Y<=x))    

F_Y=np.vectorize(F_Y)

def H(x,y):
    return(np.average((X<=x)*(Y<=y)))

H=np.vectorize(H)

n=1000
sigmaX = 2
sigmaY = 5
X=rd.normal(0,sigmaX,n)
Y=rd.normal(0,sigmaY,n)
    

Rx=2;Ry=2
dx=0.1;dy=0.1
x=np.arange(-Rx,Rx,dx) ; y=np.arange(-Ry,Ry,dy)

    
x, y = np.meshgrid(x,y)
ax = plt.axes(projection = '3d')
ax.plot_surface(x, y, F_X(x)*F_Y(y), cmap=cm.jet)


ax.plot_surface(x, y, H(x,y), cmap=cm.viridis)

plt.show()
